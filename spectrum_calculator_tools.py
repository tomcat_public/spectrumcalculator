import ipywidgets as widgets
import matplotlib.pyplot as plt
import numpy as np
import xraydb as xdb


def material_transmission(material, thickness, energy, density=None):
    """
    material: material, e.g.: 'Fe'
    thickness: material thickness in mm
    energy: X-ray energy in eV
    density: density of the material. If None, use tablulated values

    """
    try:
        # This should work if it's an element
        rho = xdb.atomic_density(material)
        mu = xdb.mu_elam(material, energy) * rho
    except:
        # This should work if it's a material
        mu = xdb.material_mu(material, energy, density)

    abs_len = 1.0e1 / mu
    trans = np.exp(-(thickness / abs_len))
    return trans


class SpectrumCalculator(widgets.Box):

    def __init__(self, energy, I0, n_filters=4):
        super().__init__()
        output = widgets.Output()

        self.energy = energy
        self.flux_I0 = I0
        self.n_filters = n_filters

        self.dE = self.energy[1] - self.energy[0]
        self.eV2J = 1.602176634e-19

        default_reference = [True, True, False, False, False] + [False] * 20
        default_materials = ['kapton', 'air', 'C', 'Mo', 'Si'] + [''] * 20
        default_thicknesses = [0.3, 500, 5.0, 0.0, 0.0] + [0.0] * 20
        default_densities = [-1.0, -1.0, -1.0, -1.0, -1.0] + [-1.0] * 20

        style = {'description_width': '50%'}
        items_layout = widgets.Layout(width='auto',
                                      flex='1 0 auto', border='none')
        hbox_layout = widgets.Layout(display='flex',
                                     flex_flow='row',
                                     flex='1 0 auto',
                                     align_items='stretch',
                                     justify_content='space-between',
                                     border='none')
        vbox_layout = widgets.Layout(display='flex',
                                     flex_flow='column',
                                     flex='1 0 auto',
                                     align_items='stretch',
                                     border='2px solid',
                                     margin='5px',
                                     padding='5px')

        filter_material_options = ([
            xdb.atomic_symbol(i) for i in range(1,99)] +
            sorted(xdb.get_materials().keys()))
        scintillator_material_options = [
            k for k,v in xdb.get_materials().items() if
            (set(v.categories) & set(['scintillator']))]

        self.w_filter_label = []
        self.w_reference = []
        self.w_material = []
        self.w_thickness = []
        self.w_density = []
        self.w_filter_set = []

        for i in range(n_filters):
            self.w_reference.append(widgets.Checkbox(
                value=default_reference[i],
                description='Is reference',
                disable=False,
                indent=False,
                layout=items_layout))
            self.w_thickness.append(widgets.FloatText(
                value=default_thicknesses[i],
                description='Thickness [mm]',
                disabled=False,
                style=style,
                layout=items_layout))
            self.w_density.append(widgets.FloatText(
                value=default_densities[i],
                description='Density [g/cm3]',
                disabled=False,
                style=style,
                layout=items_layout))
            self.w_material.append(widgets.Combobox(
                value=default_materials[i],
                description='Material',
                placeholder='Material',
                disabled=False,
                continuous_update=False,
                options=filter_material_options,
                style=style,
                layout=items_layout))
            self.w_filter_label.append(
                widgets.Label('Filter {}:'.format(i), layout=items_layout))
            self.w_filter_set.append(widgets.Box([
                self.w_filter_label[i],
                self.w_reference[i],
                self.w_material[i],
                self.w_thickness[i],
                self.w_density[i]],
                layout=hbox_layout))
            filter_set_label = widgets.Label(
                'Select here the filters to use:',
                layout=items_layout)
        self.w_filters = widgets.Box([filter_set_label] + self.w_filter_set,
            layout=vbox_layout)

        # connect the filter widgets with the update function
        for i in range(self.n_filters):
            self.w_reference[i].observe(self.update_filter_data, 'value')
            self.w_material[i].observe(self.update_filter_data, 'value')
            self.w_thickness[i].observe(self.update_filter_data, 'value')
            self.w_density[i].observe(self.update_filter_data, 'value')

        self.w_sci_label = widgets.Label('Scintillator:',
                layout=items_layout)
        self.w_sci_active = widgets.Checkbox(
                value=True,
                description='Plot?',
                disable=False,
                indent=False,
                layout=items_layout)
        self.w_sci_mat = widgets.Combobox(
                value='luag',
                description='Material',
                placeholder='Material',
                disabled=False,
                continuous_update=False,
                options=scintillator_material_options,
                style=style,
                layout=items_layout)
        self.w_sci_thick = widgets.FloatText(
                value=0.02,
                description='Thickness [mm]',
                disabled=False,
                style=style,
                layout=items_layout)
        self.w_sci_dens = widgets.FloatText(
                value=-1.0,
                description='Density [g/cm3]',
                disabled=False,
                style=style,
                layout=items_layout)
        self.w_scintillator_line = widgets.Box([
            self.w_sci_label,
            self.w_sci_active,
            self.w_sci_mat,
            self.w_sci_thick,
            self.w_sci_dens],
            layout=hbox_layout)
        w_scintillator_label = widgets.Label(
            'Select here the scitillator being used:',
            layout=items_layout)
        self.w_scintillator = widgets.Box(
            [w_scintillator_label, self.w_scintillator_line],
            layout=vbox_layout)
        self.w_scintillator.layout.border='2px solid grey'
        self.w_scintillator.layout.margin='5px'

        # connect the scintillator widgets with the update function
        self.w_sci_active.observe(self.update_scintillator_data, 'value')
        self.w_sci_mat.observe(self.update_scintillator_data, 'value')
        self.w_sci_thick.observe(self.update_scintillator_data, 'value')
        self.w_sci_dens.observe(self.update_scintillator_data, 'value')

        self.flux_I1 = self.filter_calc()
        self.flux_ref = self.filter_calc(only_ref=True)
        self.flux_sci = self.scintillator_calc()

        self.w_plot_output = widgets.Output(layout=vbox_layout)
        self.w_table_output = widgets.Output(layout=vbox_layout)

        app_layout = widgets.Layout(display='flex-basis',
                             flex_flow='column',
                             flex='0 1 auto',
                             align_items='stretch',
                             border='3px solid',
                             padding='5px',
                             width='100%')
        self.app = widgets.Box(
            [
                self.w_filters,
                self.w_scintillator,
                self.w_plot_output,
                self.w_table_output
            ],
            layout=app_layout)

        with self.w_plot_output:
            self.fig, self.ax = plt.subplots(constrained_layout=True,
                figsize=(8, 4))
            self.line_I0, = self.ax.plot(energy/1000, self.flux_I0, 'k',
                lw=2, label="I0: raw BM spectrum")
            self.line_ref, = self.ax.plot(energy/1000, self.flux_ref, 'b',
                lw=2, label="I_ref: Reference spectrum")
            self.line_I1, = self.ax.plot(energy/1000, self.flux_I1, 'r',
                lw=2, label="I1: Filtered spectrum")
            self.line_sci, = self.ax.plot(energy/1000, self.flux_sci, 'g',
                lw=2, label="I_sci: Absorbed by scintillator")
            self.ax.legend()
            self.ax.set_xlabel("X-ray energy [keV]")
            self.ax.set_ylabel("X-ray flux at POI [ph/s/eV]")
            self.ax.set_ylim([0,np.max(self.flux_ref)*1.1])

        self.update_filter_data(None)
        self.update_scintillator_data(None)

        # add to children
        self.children = [self.app]

    def update_filter_data(self, new_val):
        """
        This gets called when any of the filter widgets change.
        """
        self.flux_I1 = self.filter_calc()
        self.update_filter_line()
        self.flux_ref = self.filter_calc(only_ref=True)
        self.update_reference_line()
        self.update_scintillator_data(new_val)

    def update_scintillator_data(self, new_val):
        self.flux_sci = self.scintillator_calc()
        self.update_sci_line()
        self.update_table()

    def update_filter_line(self):
        self.line_I1.set_ydata(self.flux_I1)

    def update_reference_line(self):
        self.line_ref.set_ydata(self.flux_ref)
        self.ax.set_ylim([0,np.max(self.flux_ref)*1.1])

    def update_sci_line(self):
        if self.w_sci_active.value:
            self.line_sci.set_visible(True)
            self.line_sci.set_ydata(self.flux_sci)
        else:
            self.line_sci.set_visible(False)

    def update_table(self):
        eV2J = 1.602176634e-19

        N_I0 = np.sum(self.flux_I0) * self.dE
        N_ref = np.sum(self.flux_ref) * self.dE
        N_I1 = np.sum(self.flux_I1) * self.dE
        N_sci = np.sum(self.flux_sci) * self.dE
        P_I0 = np.sum(self.flux_I0 * self.energy) * self.dE * self.eV2J
        P_ref = np.sum(self.flux_ref * self.energy) * self.dE * self.eV2J
        P_I1 = np.sum(self.flux_I1 * self.energy) * self.dE * self.eV2J
        P_sci = np.sum(self.flux_sci * self.energy) * self.dE * self.eV2J

        N_row_labels = ["I0", "I_ref", "I1", "I_sci"]
        N = [N_I0, N_ref, N_I1, N_sci]
        rN_I0 = N / N_I0 * 100.0
        rN_ref = N / N_ref * 100.0
        rN_I1 = N / N_I1 * 100.0

        P_row_labels = ["P0", "P_ref", "P1", "P_sci"]
        P = [P_I0, P_ref, P_I1, P_sci]
        rP_I0 = P / P_I0 * 100.0
        rP_ref = P / P_ref * 100.0
        rP_I1 = P / P_I1 * 100.0

        with self.w_table_output:
            self.w_table_output.clear_output()


            print("1.) X-ray Flux at POI [ph/s] (integrated over whole "
                "plotted spectrum):")
            print("    {:10s} {:>10s} {:>10s} {:>10s} {:>10s}".format(
                " ", "Flux", "%I0", "%I_ref", "%I1"))
            N_fmt_str = "    {:10s} {:10.3e} {:10.1f} {:10.1f} {:10.1f}"
            for i,lb in enumerate(N_row_labels):
                print(N_fmt_str.format(
                    lb, N[i], rN_I0[i], rN_ref[i], rN_I1[i]))

            print("\n2.) X-ray beam power at POI [W]:")
            print("    {:10s} {:>10s} {:>10s} {:>10s} {:>10s}".format(
                " ", "Power", "%P0", "%P_ref", "%P1"))
            P_fmt_str = "    {:10s} {:10.2g} {:10.1f} {:10.1f} {:10.1f}"
            for i,lb in enumerate(P_row_labels):
                print(P_fmt_str.format(
                    lb, P[i], rP_I0[i], rP_ref[i], rP_I1[i]))

    def filter_calc(self, only_ref=False):
        """
        Calculate the effect of a number of filters on the beam spectrum
        """

        I1 = self.flux_I0.copy()
        for i in range(self.n_filters):
            dens = self.w_density[i].value
            if dens <=0:
                dens = None
            #print(material, thickness, dens)
            if self.w_material[i].value and self.w_thickness[i].value > 0:
                # Only calculate and apply the transmission if input is present
                if ((not only_ref) or self.w_reference[i].value):
                    # Only apply this filter if all filters are calculated or
                    # if it is a reference
                    t = material_transmission(self.w_material[i].value,
                        self.w_thickness[i].value, self.energy, dens)
                    I1 *= t

        return I1

    def scintillator_calc(self):
        if self.w_sci_active.value:
            dens = self.w_sci_dens.value
            if dens <=0:
                dens = None
            t_sci = material_transmission(
                self.w_sci_mat.value, self.w_sci_thick.value,
                self.energy, dens)
            abs_sci = 1.0 - t_sci
            flux_sci = self.flux_I1 * abs_sci
        else:
            flux_sci = np.zeros(len(self.flux_I1))
        return flux_sci
