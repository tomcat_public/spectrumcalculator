# TOMCAT spectrum calculator

Welcome to the git repository of the TOMCAT spectrum calculator.

# How to run the calculator
The calculator is currently written as a python jupyter notebook. In the long
run, this should be converted to a standalone application, but the notebook is
a great way to collect the first experience and perform the initial development
cycle.

There are several ways you can run the calculator, either by using a proper
python environment locally or by making use of the binder service
(see https://mybinder.org/ for more information on binder itself) to run
the notebook on a remote service in the browser window.

## Running the spectrum calculator in your browser

Click the below Binder image to launch a Jupyter Notebook with the spectrum
calculator without the need to download or install anything locally on your
machine.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.psi.ch%2Ftomcat_public%2Fspectrumcalculator/HEAD?filepath=spectrum_calculator.ipynb)

Note that it will take a few minutes to set up the remote python environment,
so please be patient.


## Running the notebook locally


### Prerequisites
In order to run the notebook locally on your computer, you need to set up a
python environment with the necessary packages. At the moment, these include

* python=3
* ipympl
* ipywidgets
* jupyter
* matplotlib
* numpy
* xraydb
* xrt


### Download the spectrum calculator

Download the spectrum calculator either by cloning the git repository (requires
a local git installation)

```bash
git clone https://gitlab.psi.ch/tomcat/spectrum_calculator.git
```

or by downloading the zip file with the latest version:

https://gitlab.psi.ch/tomcat_public/spectrumcalculator/-/archive/master/spectrumcalculator-master.zip

### Installing conda

The easiest way to create a dedicated python environment is by installing
Anaconda (https://anaconda.org/) or miniconda
(https://docs.conda.io/en/latest/miniconda.html) on your system. This provides
you with the conda environment management system.

### Install and activating the spectrum calculator environment
This step is optional, but if you like, you can install a dedicated conda
environment for the spectrum calculator that comes with the correct packages
already configured.

#### Using the command line

Locate the `environment.yml` file in this repository and run the following
command:

```bash
conda env create -f environment.yml
```
This will create a new environment called `spectrum_calculator`. Once the
installation has finished, you then need to activate this environment:

```bash
conda activate spectrum_calculator
```

#### Using Anaconda-Navigator

When using the Anaconda-Navigator, you can also install the new environment via
the GUI interface. Open the Anaconda-Navigator application, then select the
*Environments* tab in the left navigation bar. Click on the *Import* button
at the bottom of the environments list.

In the dialog window, navigate to the `environment.yml` file. The name of the
environment will be inserted automatically. The click *Import* and wait for
the installation to finish.

Now you can always activate this environment by selecting it in the
environments list.


#### Start the jupyter notebook server

In the python environment you would like to use to run the spectrum calculator,
you now need to start the jupyter notebook server:

```bash
jupyter-notebook
```

If using the Anaconda-Navigator, you can also click on the *play* symbol next
to the spectrum calculator environment and select *Open with Jupyter Notebook*.

Now either a browser will open automatically with the correct window, or you
have to copy and past the generated html link to your browser to open the
jupyter notebook service.

Inside the jupyter window in your browser, navigate to the
`spectrum_calculator.ipynb` and start the notebook.


