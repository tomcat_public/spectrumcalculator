{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Spectrum calculator for the (s-)TOMCAT beamline\n",
    "\n",
    "Welcome to a first incarnation of a spectrum calculator for the TOMCAT beamline, also aimed at being able to calculate spectra for the upcoming sTOMCAT beamline!\n",
    "\n",
    "The code here represents a quick first test application which is planned to be restructured into a standalone application at some point. For now, extensive feedback about the functionality is very welcome to plan the full feature set for the final product.\n",
    "\n",
    "The spectrum calculator is made of two parts:\n",
    "\n",
    "### 1. Source calculation\n",
    "The full spectrum of the (configurable) bending magnet source is calculated for a user-defined source distance and field of view. This is referred to as the \"raw\" spectrum, **I0**, and does not yet take any absorption effects due to filters into account.\n",
    "\n",
    "### 2. Spectrum calculation and filter transmission\n",
    "Once the raw spectrum from the source has been calculated, the effects of adding different filters and the detection efficiency of the scintillator material can be investigated. This is performed interactively in the spectrum calculator widget below."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "source": [
    "# Start off with the necessary imports\n",
    "%matplotlib widget\n",
    "\n",
    "from spectrum_calculator_tools import *\n",
    "import xrt.backends.raycing.sources as rs"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# 1. Source calculation\n",
    "The source calculation is done by using the XrayTracer library (https://xrt.readthedocs.io/). The parameters for this are to be set manually below, adjusting the values according to your needs.\n",
    "\n",
    "Notice: The below calculations have been compared to those used earlier from XOP, and they are indeed exactly identical."
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 1.1 User-defined variables\n",
    "Please modify the variables in the next cell as needed to calculated the source spectrum according to your application.\n",
    "\n",
    "Specifically, the current TOMCAT beamline and the planned sTOMCAT beamline have the following parameters source\n",
    "\n",
    "| Configuration    | Storage ring energy [GeV] | Storage ring current [A] | Superbend magnetic field strength [T] |\n",
    "| :--------------- |:---:|:---:|:---:|\n",
    "| TOMCAT (now)     | 2.4 | 400 | 2.9 |\n",
    "| sTOMCAT (SLS2.0) | 2.7 | 400 | 5.0 |\n",
    "\n",
    "Additionally, one can define the distance of the point of interest (POI) from the source as well as the transverse size of the region-of-interest (ROI), which would usually correspond to the well-slitted down field of view of the detector, at the POI."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "source": [
    "######################################################################\n",
    "# MODIFY THESE PARAMETERS AS NEEDED\n",
    "\n",
    "# Energy range for the calculation [eV]\n",
    "# (Needs to include high energies up to 0.5 MeV\n",
    "# for the total power calculations to be correct\n",
    "# in absolute numbers)\n",
    "energy = np.linspace(1, 60001, 501)\n",
    "\n",
    "# Storage ring energy [GeV] --> eE\n",
    "storage_ring_energy = 2.4\n",
    "\n",
    "# Storage ring electron current [A] --> eI\n",
    "storage_ring_current = 0.4\n",
    "\n",
    "# Bending magnet field strength [T] --> B0\n",
    "B0 = 2.9\n",
    "\n",
    "# The distance of the measurement point (POI) from the source [m]\n",
    "source_distance = 25\n",
    "\n",
    "# Horizontal and vertical ROI size at the POI [mm]\n",
    "size_h = 1.0\n",
    "size_v = 1.0\n",
    "\n",
    "######################################################################"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 1.2 Run the source calculation\n",
    "Now the source calculation can be run in the next few cells.\n",
    "\n",
    "First, the full energy and spatially resolved source spectrum at the POI is calculated."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "source": [
    "# calculate the horizontal acceptance of the ROI [rad]\n",
    "# Use only very few points for the grid as the profile shape is independent of the horizontal opening angle.\n",
    "theta_range = size_h / (source_distance * 1e3)\n",
    "theta = np.linspace(-0.5 * theta_range, 0.5 * theta_range, 3, endpoint=False)\n",
    "theta = theta + (theta[1] - theta[0]) / 2.0\n",
    "\n",
    "# calculate the vertical acceptance of the ROI [rad]\n",
    "psi_range = size_v / (source_distance * 1e3)\n",
    "psi = np.linspace(-0.5 * psi_range, 0.5 * psi_range, 51, endpoint=False)\n",
    "psi = psi + (psi[1] - psi[0]) / 2.0\n",
    "\n",
    "# calculate step sizes in theta, psi, and energy\n",
    "dtheta = theta[1] - theta[0]\n",
    "dpsi = psi[1] - psi[0]\n",
    "dE = energy[1] - energy[0]\n",
    "\n",
    "# Horizontal and vertical acceptance [mrad] --> xPrimeMax, zPrimeMax\n",
    "# I am not sure how these are actually taken into account... Changing\n",
    "# their values seems to have no effect on the rest of the calculations\n",
    "acceptance_h = 2.0\n",
    "acceptance_v = 0.3\n",
    "\n",
    "# resulting flux in units per eV (and not in 0.1% bandwidth)\n",
    "distE = 'eV'\n",
    "\n",
    "# calculate the source spectrum for the above parameters\n",
    "kwargs = dict(eE=storage_ring_energy, eI=storage_ring_current, B0=B0, distE=distE,\n",
    "              xPrimeMax=acceptance_h, zPrimeMax=acceptance_v)\n",
    "source = rs.BendingMagnet(**kwargs)\n",
    "I0xrt = source.intensities_on_mesh(energy, theta, psi)[0]\n",
    "flux_I0 = I0xrt.sum(axis=(1, 2)) * dtheta * dpsi"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Output some basic information about the source flux and power through the ROI."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "source": [
    "# Calculate the integrated flux and power\n",
    "print(\"Point of interest (POI): {} mm x {} mm @ {} m\".format(size_h, size_v, source_distance))\n",
    "I0 = np.sum(flux_I0) * dE\n",
    "print(\"Integrated flux at POI from BM [ph/s]: {:.3g}\".format(I0))\n",
    "eV2J = 1.602176634e-19\n",
    "E0 = np.sum(flux_I0 * energy) * dE * eV2J\n",
    "print(\"Total power at POI from BM [W]: {:.3g}\".format(E0))"
   ],
   "outputs": [
    {
     "output_type": "stream",
     "name": "stdout",
     "text": [
      "Point of interest (POI): 1.0 mm x 1.0 mm @ 25 m\n",
      "Integrated flux at POI from BM [ph/s]: 2.63e+14\n",
      "Total power at POI from BM [W]: 0.324\n"
     ]
    }
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Plot the spatially resolved beam profile, integrated over all energies. Obviously, the beam profile at different energies would be different, with a broader distribution at low energies and a narrower profile at high energies."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "source": [
    "# Plot the transverse beam profile integrated over all energies at the point of interest\n",
    "beam_profile = I0xrt.sum(axis=0) * dE * dpsi * dtheta\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(figsize=[10, 4], ncols=2)\n",
    "plt.axes(ax1)\n",
    "extent = [-size_h/2.0, size_h/2.0, -size_v/2.0, size_v/2.0]\n",
    "plt.imshow(beam_profile.T, extent=extent, interpolation='bilinear')\n",
    "plt.axis('equal')\n",
    "plt.axis('tight')\n",
    "plt.title('X-ray beam profile at point of interest\\n(Flux integrated over all energies)')\n",
    "plt.xlabel('Horizontal position [mm]')\n",
    "plt.ylabel('Vertical position [mm]')\n",
    "plt.colorbar()\n",
    "\n",
    "plt.axes(ax2)\n",
    "bp = beam_profile[0,:]\n",
    "plt.plot(bp, label='all energies')\n",
    "plt.ylim([0, bp.max()*1.1])\n",
    "plt.title('Vertical beam profile')\n",
    "plt.xlabel('Vertical position [mm]')\n",
    "plt.show()"
   ],
   "outputs": [
    {
     "output_type": "display_data",
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "2e52d2afa4a5440698966c7b1530b7c8",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {}
    }
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# 2. Spectrum calculator\n",
    "\n",
    "## 2.1 Filter and scintillator materials\n",
    "Filter transmission and scintillator absorption calculations are carried out using the XrayDB library (https://xraypy.github.io/XrayDB/).\n",
    "\n",
    "In general, one needs to define three parameters for each filter/scintillator:\n",
    "1. The **material**: The atomic composition (chemical formula) of the material to be used.\n",
    "1. The **thickness**: The thickness of the material along the beam direction [mm].\n",
    "1. The **density**: The density of the material [g/cm3].\n",
    "\n",
    "A large number of materials is already preconfigured in the database, and a few more scintillator materials are defined and added in the cell below. This includes all of the natural elements as well. All pre-configured materials are selectable through the combobox dropdown menues.\n",
    "\n",
    "If the desired material is not available in the combox, it can be entered manually by specifying the atomic composition as one string without spaces. E.g.: sapphire = `Al2O3`. Note that also relative compositions with non-integer coefficients are possible, for example for metal alloys: `Al0.78Mn0.1Ni0.12`.\n",
    "\n",
    "Densities for the preconfigured materials do not need to be specified explicitly if you trust the tabulated values. Simply set the value in the density field to `-1`. For all user-specified compounds (which are not selectable in the drop-down), you must specify the density, however. Also note that certain elements can have various solid forms (e.g.: carbon) with different densities.\n",
    "\n",
    "Note: a nice web-interface to the XrayDB data is available here: https://xraydb.xrayabsorption.org/"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "source": [
    "# Add custom materials to the list here.\n",
    "# Entries are tuples that contain four fields:\n",
    "#      1.  common name\n",
    "#      2.  chemical formula\n",
    "#      3.  density in gr/cm^3\n",
    "#      4.  comma delimited list of categories\n",
    "\n",
    "materials = [\n",
    "    # Name, Formula, Density, Categories\n",
    "    ('Sigradur G glassy carbon', 'C', 1.42, ['element']),\n",
    "    ('CVD diamond', 'C', 3.52, ['element']),\n",
    "    ('LuAG', 'Al5Lu3O12', 6.71, ['ceramic', 'scintillator']),\n",
    "    ('LSO', 'Lu2SiO5', 7.4, ['ceramic', 'scintillator']),\n",
    "    ('LYSO', 'Lu1.8Y0.2SiO5', 7.1, ['ceramic', 'scintillator']),\n",
    "    ('GGG', 'Gd3Ga5O12', 7.08, ['ceramic', 'scintillator']),\n",
    "    ('borosilicate glass', '(B2O3)0.12(SiO2)0.88', 2.51, ['glass'])\n",
    "]\n",
    "\n",
    "# Add the materials to the database\n",
    "for material in materials:\n",
    "    name, formula, density, categories = material\n",
    "    if not xdb.find_material(name):\n",
    "        xdb.add_material(name, formula, density, categories)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## 2.2 Launch the calculator\n",
    "\n",
    "The spectrum calculator diplays up to four different spectra for comparison:\n",
    "1. The raw spectrum from the source, **I0**.\n",
    "1. Based on the raw spectrum, one can define a reference spectrum, **I_ref** (for example taking into account the fixed elements in the beam path, such as diamond CVD windows, kapton windows, air, etc.). Only those filters marked as *reference* in the corresponding checkbox are included in this calculation.\n",
    "1. The filtered spectrum, **I1**. This is calculated based on the raw spectrum and all of the specified filters (those with valid material specifications and a positive non-zero thickness value).\n",
    "1. The part of the filtered spectrum which gets absorbed by the scintillator, **I_sci**. This can be disabled by unselecting the *Plot?* checkbox for the scintillator\n",
    "\n",
    "Changing any of the numbers, checkboxes, or materials triggers a recalculation of the plot below the parameter sections and the information section below the plot.\n",
    "\n",
    "The information section shows two overview tables, detailing the overall flux [ph/s] and X-ray beam power [W] for each of the plotted spectra, as well as their relative magnitudes, references to **I0**, **I_ref**, and **I1**, respectively in each column.\n",
    "\n",
    "The number of available filters to use in the calculation can be specified in the `SpectrumCalculator` initialization call below (up to 25 filters are supported in theory)."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "source": [
    "# Launch the spectrum calculator\n",
    "SpectrumCalculator(energy, flux_I0, n_filters=6)"
   ],
   "outputs": [
    {
     "output_type": "display_data",
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "e7fa9138776643cc914a71feb9c8112a",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "SpectrumCalculator(children=(Box(children=(Box(children=(Label(value='Select here the filters to use:', layout…"
      ]
     },
     "metadata": {}
    }
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Ideas for improvements\n",
    "\n",
    "* Add an input field to enter a small number of energies, and output the flux values at these energies for all beam conditions (maybe define one as the reference energy and give also percentages of the others with respect to this reference)\n",
    "* Calculate and display integrated flux below and above the reference energy value (for all spectra)\n",
    "* Add plots of the vertical beam flux profiles for all beam conditions (no filter, reference filter, all filters, with scintillator detection capabilities)\n",
    "* Add on-axis and farthest off-axis spectra as \"confidence\" intervals to the spectrum lines to represent the intensity spread in the FOV at each energy\n",
    "* Add multilayer monochromator as an optical element into the calculation.\n",
    "* functions to export the data"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "source": [
    "plt.figure(2)\n",
    "#plt.gca().get_legend().remove()\n",
    "plt.gca().set_xlabel(\"\")"
   ],
   "outputs": [
    {
     "output_type": "execute_result",
     "data": {
      "text/plain": [
       "Text(0.5, 18.166999999999994, '')"
      ]
     },
     "metadata": {},
     "execution_count": 16
    }
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [],
   "outputs": [],
   "metadata": {}
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}